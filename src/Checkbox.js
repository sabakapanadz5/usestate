import {useState} from "react"
import "./Checkbox.css"
const Checkbox = ({title ,selectedbox }) => {
 const [selected , setselected] = useState(selectedbox)
    const checkboxchanged = (event) =>{
      setselected(event.target.checked)
    }
    return(
  <div className="Checkbox">
  <input 
   type="checkbox"
   checked = {selected} 
   onChange={checkboxchanged}>
   </input>
  <label className={(selected ? ` label_blue` : `` )}>{title}</label><br></br>
  </div>
    )
}
export default Checkbox
