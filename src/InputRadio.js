import "./Checkbox.css"
import {useState} from "react"
import "./InputRadio.css"
const InputRadio = ({title }) =>{
    const [selected, setselected] = useState()
    const radiochanged = (value) =>{
      setselected(true)
    }
    return(
        <div className= "Radio">
           <input type="radio" 
           name="age"
           onChange={radiochanged}></input>
           <label className={(selected ? ` label_blue` : `` )} 
>
           {title}</label><br></br>
        </div>
    )
}
export default InputRadio

