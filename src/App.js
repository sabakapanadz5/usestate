
import './App.css';
import Checkbox from './Checkbox';
import InputRadio from './InputRadio';
import InputText from './InputText';
import TextArea from './TextArea';

function App() {
  return (
    <main className="main">
  <InputText/>
  <InputRadio title = "male"/>
  <InputRadio title = "female"/>
  <InputRadio  title = "other"/>
  <Checkbox selectedbox blue title =" My name is Mickey Mouse"/>
  <Checkbox title =" My name is Buzz Lightyear"/>
  <Checkbox title =" My name is Shrek"/>
  <TextArea/>
  </main>

  
  );
}

export default App;
